import PropTypes from 'prop-types';
import React from 'react';
import { DivFloatLabel, InputText, LabelNormal } from './style';

const InputCustom = React.forwardRef((props, ref) => (
  <DivFloatLabel>
    <InputText {...props} ref={ref} style={{ borderRadius: '8px' }}>
      {props.children}
    </InputText>
    <LabelNormal> {props.label}</LabelNormal>
  </DivFloatLabel>
));

InputCustom.propTypes = {
  label: PropTypes.string,
  children: PropTypes.any,
};

export default InputCustom;
