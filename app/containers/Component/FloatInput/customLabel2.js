import styled from 'styled-components';

const CustomLabel2 = styled.label`
  z-index: 1;
  font-weight: normal;
  position: absolute;
  pointer-events: none;
  left: 12px;
  top: 11px;
  font-size: 14px;
  transition: 0.2s ease all;
  color: #a8b1bd;
`;

export default CustomLabel2;
