import React, { useState } from 'react';
import CustomLabel2 from './customLabel2';
import InputDiv from './InputDiv';
import CustomLabel from './customLabel';
import CustomInput from './CustomInput';

const FloatInput = props => {
  const [focus, setFocus] = useState(false);
  const [haveText, sethaveText] = useState(false);
  // eslint-disable-next-line prefer-const,react/prop-types
  let { label, value, placeholder, type } = props;
  if (!placeholder) placeholder = label;
  const isOccupied = focus || haveText;
  const handleChange = e => {
    console.log(e.target.value);
    if (e.target.value.length > 0) {
      sethaveText(true);
    } else {
      sethaveText(false);
    }
  };
  return (
    <InputDiv onBlur={() => setFocus(false)} onFocus={() => setFocus(true)}>
      <CustomInput onChange={handleChange} type={type} defaultValue={value} />
      {isOccupied ? (
        <CustomLabel>
          {placeholder} <span style={{ color: 'red' }}>*</span>
        </CustomLabel>
      ) : (
        <CustomLabel2>
          {placeholder} <span style={{ color: 'red' }}>*</span>
        </CustomLabel2>
      )}
    </InputDiv>
  );
};

export default FloatInput;
