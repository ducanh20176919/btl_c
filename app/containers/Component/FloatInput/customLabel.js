import styled from 'styled-components';

const CustomLabel = styled.label`
  z-index: 1;
  font-weight: normal;
  position: absolute;
  pointer-events: none;
  left: 12px;
  top: -10px;
  font-size: 12px !important;
  background: white;
  padding: 0 4px;
  color: #40a9ff;
  margin: auto;
`;

export default CustomLabel;
