import styled from 'styled-components';
import { Input } from 'antd';

const CustomInput = styled(Input)`
  border-radius: 12px;
  height: 42px;
  .ant-form-item-has-error {
    margin-bottom: 24px;
  }
`;

export default CustomInput;
