import styled from 'styled-components';
import { Select } from 'antd';

const DivFloatLabel = styled.div`
  position: relative;
  color: #212529;
  label:after {
    content: ' ';
    display: block;
    position: absolute;
    background: #ffffff;
    height: 4px;
    top: 50%;
    left: -0.1em;
    right: -0.2em;
    z-index: -1;
  }
`;

const SelectOptions = styled(Select)`
  height: 42px;
  width: 100%;
  &.ant-select {
    height: 42px;
    border-radius: 12px;
  }
`;

const Label = styled.label`
  position: absolute;
  pointer-events: none;
  left: 12px;
  top: 11px;
  transition: 0.2s ease all;
  font-style: normal;
  font-weight: 400;
  font-size: 1em;
  line-height: 22px;
  color: rgba(0, 0, 0, 0.25);
`;

const LabelNormal = styled(Label)`
  z-index: 2;
  top: -8px;
  padding: 0 4px;
  margin-left: -4px;
  font-style: normal;
  font-weight: 400;
  font-size: 0.85714em;
  line-height: 16px;
  color: #000000;
`;

export { DivFloatLabel, SelectOptions, LabelNormal };
