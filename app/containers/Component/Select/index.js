import PropTypes from 'prop-types';
import React from 'react';
import { DivFloatLabel, SelectOptions, LabelNormal } from './style';

const SelectCustom = React.forwardRef((props, ref) => (
  <DivFloatLabel>
    <SelectOptions {...props} ref={ref} style={{ borderRadius: '8px' }}>
      {props.children}
    </SelectOptions>
    <LabelNormal> {props.label}</LabelNormal>
  </DivFloatLabel>
));

SelectCustom.propTypes = {
  label: PropTypes.string,
  children: PropTypes.any,
};

export default SelectCustom;
