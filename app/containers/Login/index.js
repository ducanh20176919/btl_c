/**
 * ...
 */

import React from 'react';
import { Button, Checkbox, Form, Input } from 'antd';
import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import { useDispatch } from 'react-redux';
import Cookies from 'js-cookie';
import { Container, Wrapper } from './style';
import LoginForm from './Form';
import * as actions from './actionsLogin';
import reducer from './reducerLogin';
import saga from './sagaLogin';
import { REDUX_KEY } from './constantsLogin';
import history from '../../utils/history';
import 'react-toastify/dist/ReactToastify.css';
import { NotificationPopup } from '../../res/components/NotificationPopup';
import { ERROR } from '../../res/components/NotificationPopup/constants';
import { COOKIES } from '../../utils/constants';
const key = REDUX_KEY;

const Login = () => {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const dispatch = useDispatch();
  const checkPasswordInvalid = password => {
    // eslint-disable-next-line no-useless-escape
    const regex = /^(?!.*\s)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[~`!@#$%^&*()--+={}\[\]|\\:;"'<>,.?/_₹]).{8,99}$/;
    const checkValid = regex.test(password);
    return checkValid;
  };

  const onFinish = values => {
    if (values.remember && checkPasswordInvalid(values.password)) {
      const body = {
        username: values.username,
        password: values.password,
      };
      dispatch(
        actions.login(body, res => {
          if (res.status === 200) {
            Cookies.set(COOKIES.access_token, res.data.access_token);
            history.push('/user-management');
          }
        }),
      );
    } else if (!values.remember) {
      NotificationPopup('Bạn phải đồng ý điều khoản sử dụng', ERROR);
    } else if (values.password.length < 10) {
      NotificationPopup('Mật khẩu đăng nhập phải có độ dài lớn hơn 8', ERROR);
    } else {
      NotificationPopup(
        'Mật khẩu đăng nhập phải có Chữ hoa, chữ thường, kí tự đặc biệt',
        ERROR,
      );
    }
  };
  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Container>
      <Wrapper>
        <LoginForm
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <div className="labelForm">
            <h2>
              <strong>Đăng nhập hệ thống đại lý</strong>
            </h2>
          </div>
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập tên đăng nhập!',
              },
            ]}
            style={{
              marginBottom: '20px',
            }}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: 'Vui lòng điền mật khẩu!',
              },
            ]}
            style={{
              marginBottom: '20px',
            }}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Checkbox>
              {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
              Đồng ý với <a>điều khoản sửu dụng</a>
            </Checkbox>
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Đăng nhập
            </Button>
          </Form.Item>
          <div className="more-options">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a className="item" href="" style={{ textAlign: 'left' }}>
              Đăng ký
            </a>

            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a className="item" href="" style={{ textAlign: 'right' }}>
              Quên mật khẩu
            </a>
          </div>
        </LoginForm>
      </Wrapper>
    </Container>
  );
};

export default Login;
