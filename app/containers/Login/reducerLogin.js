import produce from 'immer';
import { LOGIN, LOGIN_FAILED, LOGIN_SUCCESS } from './constantsLogin';

export const initialState = {
  isLoading: false,
  infoUser: {},
};

const loginReducer = (state = initialState, action) =>
  produce(state, draft => {
    // eslint-disable-next-line default-case
    switch (action.type) {
      case LOGIN:
        // eslint-disable-next-line no-param-reassign
        draft.isLoading = true;
        break;

      case LOGIN_FAILED:
        // eslint-disable-next-line no-param-reassign
        draft.isLoading = false;
      // eslint-disable-next-line no-fallthrough
      case LOGIN_SUCCESS:
        // eslint-disable-next-line no-param-reassign
        draft.isLoading = false;
        // eslint-disable-next-line no-param-reassign
        draft.infoUser = action.data;
        break;
    }
  });

export default loginReducer;
