import { LOGIN } from './constantsLogin';

export function login(body, callback) {
  return {
    type: LOGIN,
    body,
    callback,
  };
}
