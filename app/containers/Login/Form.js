import styled from 'styled-components';
import { Form } from 'antd';

const LoginForm = styled(Form)`
  width: 90%;
  margin: auto;
  .ant-btn-primary {
    height: 40px;
    border-radius: 8px;
    margin-top: 15px;
    width: 100%;
    color: #fff;
    border-color: #ff6e01;
    background: #ff6e01;
    text-shadow: 0 -1px 0 rgb(0 0 0 / 12%);
    box-shadow: 0 2px 0 rgb(0 0 0 / 5%);
  }
  .more-options {
    display: flex;
    width: 100%;
  }
  .item {
    flex: 1;
  }

  .ant-col-16 {
    display: block;
    flex: 0 0 100%;
    max-width: 100%;
  }

  .ant-col-offset-8 {
    margin-left: 0;
  }

  .ant-input {
    border-radius: 8px;
    height: 40px;
  }

  .ant-input-affix-wrapper {
    border-radius: 8px;
    padding: 0px 11px;
  }

  .labelForm {
    text-align: center;
  }
`;

export default LoginForm;
