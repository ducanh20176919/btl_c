/**
 * ...
 */

import styled from 'styled-components';

const Container = styled.div`
  background-image: src('background.png');
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
  background-color: orange;
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 400px;
  height: 400px;
  border-radius: 20px;
  background-color: white;
`;

const LoginCard = styled.div`
  display: flex;
  flex-direction: row;
  width: 50%;
  height: 50%;
  margin: 10% auto auto;
`;

export { Container, LoginCard };
