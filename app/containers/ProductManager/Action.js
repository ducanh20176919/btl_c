import * as CONSTANT from './Constant';

export function requestFalse() {
  return {
    type: CONSTANT.REQUEST_FAlSE,
  };
}

export function getListProduct(body, callback) {
  return {
    type: CONSTANT.GET_LIST_PRODUCT,
    body,
    callback,
  };
}

export function getListProductSuccess(data) {
  return {
    type: CONSTANT.GET_LIST_PRODUCT_SUCCESS,
    data,
  };
}

export function addProduct(data, callback) {
  return {
    type: CONSTANT.ADD_PRODUCT,
    data,
    callback,
  };
}

export function addProductSuccess(data) {
  return {
    type: CONSTANT.ADD_PRODUCT_SUCCESS,
    data,
  };
}

export function editProduct(data) {
  return {
    type: CONSTANT.EDIT_PRODUCT,
    data,
  };
}

export function editProductSuccess(data) {
  return {
    type: CONSTANT.EDIT_PRODUCT_SUCCESS,
    data,
  };
}

export function delProduct(data) {
  return {
    type: CONSTANT.DEL_PRODUCT,
    data,
  };
}

export function delProductSuccess(data) {
  return {
    type: CONSTANT.DEL_PRODUCT_SUCCESS,
    data,
  };
}

export function getInfoProduct(data) {
  return {
    type: CONSTANT.GET_INFO_PRODUCT,
    data,
  };
}

export function getInfoProductSuccess(data) {
  return {
    type: CONSTANT.GET_INFO_PRODUCT_SUCCESS,
    data,
  };
}
