import React, { useEffect } from 'react';
import { DatePicker, Form } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment/moment';
import { CustomModal, CustomBtns, ButtonCancel, ButtonAdd } from './style';

import InputCustom from '../../../Component/Input';
import { useInjectReducer } from '../../../../utils/injectReducer';
import reducer from '../../reducer';
import { useInjectSaga } from '../../../../utils/injectSaga';
import saga from '../../saga';
import { REDUX_KEY } from '../../../../utils/constants';
import * as selector from '../../selector';
import * as actions from '../../Action';

const key = REDUX_KEY.productManagement;

// eslint-disable-next-line react/prop-types
const ProductAddAndEditModal = ({ trigger, setTrigger, type, UUID }) => {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  useEffect(() => {
    if (UUID) {
      dispatch(actions.getInfoProduct(UUID));
    }
  }, [setTrigger, UUID]);

  const info = useSelector(selector.selectInfoProduct());

  useEffect(() => {
    if (type === 1) {
      form.setFieldsValue({
        SerialNumber: '',
        Phone: '',
        RegisterCode: '',
        Name: '',
        Email: '',
        Address: '',
        SellTime: moment(new Date()),
      });
    }
    if (type === 2 && info) {
      form.setFieldsValue({
        Phone: info.Phone,
        SerialNumber: info.SerialNumber,
        RegisterCode: info.RegisterCode,
        Name: info.Name,
        Email: info.Email,
        Address: info.Address,
        SellTime: moment(info.SellTime),
      });
    }
  }, [type, info]);

  const handleSubmit = () => {
    form.validateFields().then(value => {
      if (type === 1) {
        dispatch(
          actions.addProduct(value, () => {
            console.log('add success');
          }),
        );
      }
      if (type === 2) {
        dispatch(actions.editProduct({ ...value, InfoGUID: info.InfoGUID }));
      }
    });
    setTrigger(false);
  };

  const handleCancel = () => {
    setTrigger(false);
  };

  return (
    <>
      {/* eslint-disable-next-line react/prop-types */}
      <CustomModal
        onCancel={handleCancel}
        visible={trigger}
        open={trigger}
        footer={null}
        title={type === 1 ? 'Thêm mới sản phẩm' : 'Sửa thông tin sản phẩm'}
      >
        <Form form={form}>
          <Form.Item name="SerialNumber">
            <InputCustom
              label={
                <span>
                  Mã khách hàng<span style={{ color: 'red' }}>*</span>
                </span>
              }
              placeholder="Nhập mã khách hàng"
            />
          </Form.Item>
          <Form.Item name="Phone">
            <InputCustom
              label={
                <span>
                  Số điện thoại<span style={{ color: 'red' }}>*</span>
                </span>
              }
              placeholder="Nhập số điện thoại"
            />
          </Form.Item>
          <Form.Item name="RegisterCode">
            <InputCustom
              label={
                <span>
                  Mã đăng ký<span style={{ color: 'red' }}>*</span>
                </span>
              }
              placeholder="Nhập mã đăng ký theo cu phap 'xxxx-xxxx-xxxx'"
            />
          </Form.Item>
          <Form.Item name="Name">
            <InputCustom
              label={
                <span>
                  Tên khách hàng<span style={{ color: 'red' }}>*</span>
                </span>
              }
              placeholder="Nhập tên khách hàng"
            />
          </Form.Item>
          <Form.Item name="SellTime">
            <DatePicker format="DD/MM/YYYY" />
          </Form.Item>
          <Form.Item name="Email">
            <InputCustom
              label={
                <span>
                  Email<span style={{ color: 'red' }}>*</span>
                </span>
              }
              placeholder="Nhập Email"
            />
          </Form.Item>
          <Form.Item name="Address">
            <InputCustom
              label={
                <span>
                  Địa chỉ<span style={{ color: 'red' }}>*</span>
                </span>
              }
              placeholder="Nhập địa chỉ"
            />
          </Form.Item>
        </Form>
        <CustomBtns>
          <ButtonAdd onClick={handleSubmit}>Ghi lại (Ctrl + S)</ButtonAdd>
          <ButtonCancel onClick={handleCancel}>Đóng (ESC)</ButtonCancel>
        </CustomBtns>
      </CustomModal>
    </>
  );
};

export default ProductAddAndEditModal;
