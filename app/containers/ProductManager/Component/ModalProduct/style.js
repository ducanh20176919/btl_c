import styled from 'styled-components';
import { Button, DatePicker, Form, Modal } from 'antd';

const CustomSelect = styled(Form)`
  .ant-form-item-label > label {
    font-size: 14px;
    position: relative;
    display: inline-flex;
    align-items: center;
    max-width: 100%;
    height: 32px;
    color: rgba(0, 0, 0, 0.85);
    font-size: 14px;
    font-family: 'Open Sans';
    font-weight: 700;
  }
`;

const ButtonAdd = styled(Button)`
  background-color: rgb(255, 161, 58);
  border-color: rgb(255, 161, 58);
  color: white;
  margin: 5px;
  padding: 5px 10px;
  border-radius: 12px;
  height: 40px;
  width: 118px;
`;

const CustomTimePicker = styled(DatePicker)`
  .ant-picker {
    witdh: 100%;
    height: 40px;
    border-radius: 12px;
  }
`;

const ButtonCancel = styled(Button)`
  background-color: white;
  color: rgb(255, 161, 58);
  border-color: rgb(255, 161, 58);
  margin: 5px;
  padding: 5px 10px;
  border-radius: 12px;
  height: 40px;
  width: 89px;
`;

const CustomBtns = styled.div`
  display: flex;
  justify-content: center;
`;

const warningSpan = styled.div`
  color: red;
`;

const CustomModal = styled(Modal)`
  width: 496px;
  .ant-modal-header {
    padding: 16px 24px;
    color: rgba(0, 0, 0, 0.85);
    background: #c5ced9;
    border-bottom: 1px solid #f0f0f0;
    border-radius: 20px 20px 0 0;
    text-align: center;
  }

  .ant-modal-content {
    border-radius: 20px;
  }

  .ant-radio-inner::after {
    background-color: rgb(250, 139, 52);
  }

  .ant-radio-checked .ant-radio-inner {
    border-color: rgb(250, 139, 52);
  }

  .ant-modal-title {
    margin: 0;
    color: rgba(0, 0, 0, 0.85);
    font-weight: 700;
    font-size: 18px;
    line-height: 22px;
    word-wrap: break-word;
  }

  .ant-picker {
    width: 100%;
    height: 100%;
    border-radius: 12px;
  }

  .ant-picker-input {
    position: relative;
    display: inline-flex;
    align-items: center;
    width: 100%;
    height: 36px;
    padding: 0px;
  }
`;

export {
  CustomSelect,
  warningSpan,
  ButtonAdd,
  ButtonCancel,
  CustomBtns,
  CustomModal,
  CustomTimePicker,
};
