import React from 'react';
import { useDispatch } from 'react-redux';
import { useInjectReducer } from '../../../../utils/injectReducer';
import reducer from '../../reducer';
import { useInjectSaga } from '../../../../utils/injectSaga';
import saga from '../../saga';
import { REDUX_KEY } from '../../../../utils/constants';
import {
  ButtonAdd,
  ButtonCancel,
  CustomBtns,
  CustomModal,
} from '../ModalProduct/style';
import * as actions from '../../Action';

const key = REDUX_KEY.productManagement;
// eslint-disable-next-line react/prop-types
const ModalConfirmDel = ({ trigger, setTrigger, data }) => {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  const dispatch = useDispatch();
  const handleCancel = () => {
    setTrigger(false);
  };
  const handleSubmit = () => {
    dispatch(actions.delProduct(data.InfoGUID));
    setTrigger(false);
  };
  return (
    <CustomModal
      title="Xóa thẻ"
      visible={trigger}
      open={trigger}
      onCancel={handleCancel}
      footer={null}
    >
      {data !== null ? (
        <p style={{ textAlign: 'center', fontSize: '18px' }}>
          Bạn có chắc chắn là muốn xóa thẻ <strong>{data.SerialNumber}</strong>{' '}
          không?
        </p>
      ) : null}
      <CustomBtns>
        <ButtonCancel onClick={handleCancel}>Đóng (ESC)</ButtonCancel>
        <ButtonAdd onClick={handleSubmit}>Ghi lại (Ctrl + S)</ButtonAdd>
      </CustomBtns>
    </CustomModal>
  );
};

export default ModalConfirmDel;
