import produce from 'immer';
import {
  ADD_PRODUCT,
  ADD_PRODUCT_SUCCESS,
  DEL_PRODUCT,
  DEL_PRODUCT_SUCCESS,
  EDIT_PRODUCT,
  EDIT_PRODUCT_SUCCESS,
  GET_INFO_PRODUCT,
  GET_INFO_PRODUCT_SUCCESS,
  GET_LIST_PRODUCT,
  GET_LIST_PRODUCT_SUCCESS,
} from './Constant';

export const initialState = {
  isLoading: false,
  totalRecord: 0,
  listProduct: [],
  isAddProduct: false,
  isEditProduct: false,
  isDelProduct: false,
  infoProduct: [],
};

/* eslint-disable default-case, no-param-reassign */
const productManagementReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_INFO_PRODUCT:
        draft.isLoading = true;
        break;
      case GET_LIST_PRODUCT:
        draft.isLoading = true;
        break;
      case ADD_PRODUCT:
        draft.isLoading = true;
        break;
      case EDIT_PRODUCT:
        draft.isLoading = true;
        break;
      case DEL_PRODUCT:
        draft.isLoading = true;
        draft.isDelProduct = true;
        break;

      case GET_LIST_PRODUCT_SUCCESS:
        draft.isLoading = false;
        draft.listProduct = action.data.data.BkproInfo;
        break;
      case ADD_PRODUCT_SUCCESS:
        draft.isLoading = false;
        draft.isAddProduct = true;
        break;
      case EDIT_PRODUCT_SUCCESS:
        draft.isLoading = false;
        draft.isEditProduct = true;
        break;
      case DEL_PRODUCT_SUCCESS:
        draft.isLoading = false;
        draft.isDelProduct = true;
        break;
      case GET_INFO_PRODUCT_SUCCESS:
        draft.isLoading = false;
        draft.infoProduct = action.data.data;
        break;
    }
  });

export default productManagementReducer;
