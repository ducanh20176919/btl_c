import { call, put, takeLatest } from 'redux-saga/effects';

import * as constants from './Constant';
import * as actions from './Action';

import { axiosDelete, axiosGet, axiosPost } from '../../utils/request';
// ok
export function* handleGetList(action) {
  const path = `bkpro/v2/info/list`;
  try {
    const res = yield call(axiosPost, path, action.body);
    if (res.status === 200) {
      yield put(actions.getListProductSuccess(res));
    }
  } catch (error) {
    yield put(actions.requestFalse(error));
  }
}

export function* handleAdd(action) {
  const path = `bkpro/v2/info/add`;
  console.log(action.data);
  try {
    const res = yield call(axiosPost, path, action.data);
    if (res.status === 200) {
      yield put(actions.addProductSuccess(res));
      action.callback();
    }
  } catch (error) {
    yield put(actions.requestFalse(error));
  }
}

export function* handleEdit(action) {
  const path = `bkpro/v2/info/edit`;
  try {
    const res = yield call(axiosPost, path, action.data);
    if (res.status === 200) {
      // Load lại danh sách
      yield put(actions.editProductSuccess(res));
    }
  } catch (error) {
    yield put(actions.requestFalse(error));
  }
}

// ok
export function* handleDel(action) {
  const path = `bkpro/info/delete?InfoGUID=${action.data}`;
  try {
    const res = yield call(axiosDelete, path, action.data);
    if (res.status === 200) {
      yield put(actions.delProductSuccess(res));
    }
  } catch (error) {
    yield put(actions.requestFalse(error));
  }
}
// ok
export function* handleGetInfo(action) {
  const path = `bkpro/info/infomation?InfoGUID=${action.data}`;
  try {
    const res = yield call(axiosGet, path);
    if (res.status === 200) {
      yield put(actions.getInfoProductSuccess(res));
    }
  } catch (err) {
    yield put(actions.requestFalse());
  }
}

export default function* WatchProduct() {
  yield takeLatest(constants.GET_LIST_PRODUCT, handleGetList);
  yield takeLatest(constants.ADD_PRODUCT, handleAdd);
  yield takeLatest(constants.EDIT_PRODUCT, handleEdit);
  yield takeLatest(constants.DEL_PRODUCT, handleDel);
  yield takeLatest(constants.GET_INFO_PRODUCT, handleGetInfo);
}
