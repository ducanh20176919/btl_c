import { createSelector } from 'reselect';
import { REDUX_KEY } from '../../utils/constants';
import { initialState } from './reducer';

export const selectProductManagement = state =>
  state[REDUX_KEY.productManagement] || initialState;

export function selectLoading() {
  return createSelector(
    selectProductManagement,
    state => state.isLoading,
  );
}

export function selectListProduct() {
  return createSelector(
    selectProductManagement,
    state => state.listProduct,
  );
}

export function selectInfoProduct() {
  return createSelector(
    selectProductManagement,
    state => state.infoProduct,
  );
}
