import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { Content } from 'antd/es/layout/layout';
import { PlusOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import {
  UserManagerLayout,
  SearchUser,
  UserTable,
  Toolbar,
  CustomButtonAdd,
} from '../App/style';
import ProductAddAndEditModal from './Component/ModalProduct/ProductAddAndEditModal';
import editIcon from '../../images/icon/edit.svg';
import restoreIcon from '../../images/icon/iconRestore.svg';
import deleteIcon from '../../images/icon/delete.svg';
import { REDUX_KEY } from '../../utils/constants';
import { useInjectReducer } from '../../utils/injectReducer';
import reducer from './reducer';
import { useInjectSaga } from '../../utils/injectSaga';
import saga from './saga';
import * as selector from './selector';
import * as actions from './Action';
import ModalConfirmDel from './Component/ModalConfirmDelProduct';

const body = {
  KeySearch: '',
  CardTypeID: 0,
  StatusActiveID: 0,
  StatusCountPoinID: 0,
  TimeoutID: 0,
  StartTime: '',
  EndTime: '',
  UserID: 0,
  SerialNumber: '',
  Name: '',
  Phone: '',
  PageSize: 50,
  CurrentPage: 1,
  SortCol: '',
  IsDesc: false,
};

const pageSize = 10;

const key = REDUX_KEY.productManagement;

const ProductManager = () => {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const dispatch = useDispatch();
  const listProduct = useSelector(selector.selectListProduct());
  // eslint-disable-next-line no-unused-expressions
  listProduct &&
    // eslint-disable-next-line array-callback-return
    listProduct.map(item => {
      const date = moment(new Date(item.SellTime));
      // eslint-disable-next-line no-param-reassign
      item.SellTime = date.format('DD/MM/YYYY');
    });
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalConfirmOpen, setIsModalConfirmOpen] = useState(false);
  const [keySearch, setKeySearch] = useState('');
  const [type, setType] = useState(1);
  const renderListProduct = dataInput => {
    dispatch(actions.getListProduct(dataInput));
  };

  useEffect(() => {
    const time = setTimeout(() => {
      renderListProduct({
        ...body,
        KeySearch: keySearch,
      });
    }, 300);
    return () => clearTimeout(time);
  }, [keySearch, isModalOpen]);

  const columns = [
    {
      title: 'STT',
      width: '4%',
      render: record => (
        <div>{(Number(listProduct.indexOf(record)) % pageSize) + 1}</div>
      ),
    },
    {
      title: 'Mã khách hàng',
      dataIndex: 'SerialNumber',
      width: '8%',
    },
    {
      title: 'Tên khách hàng',
      dataIndex: 'Name',
      width: '10%',
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'Phone',
      width: '12%',
    },
    {
      title: 'Mã đăng ký',
      dataIndex: 'RegisterCode',
      width: '12%',
    },
    {
      title: 'Loại thẻ',
      dataIndex: 'NameCardType',
      width: '10%',
    },
    {
      title: 'Ngày bán',
      dataIndex: 'SellTime',
      width: '12%',
    },
    {
      title: 'Thời hạn',
      dataIndex: 'Timeout',
      width: '12%',
    },
    {
      title: 'Tài khoản',
      dataIndex: 'UserName',
      width: '10%',
    },
    {
      width: '10%',
      title: 'Chức năng',
      dataIndex: 'key',
      render: (text, record) => (
        <div>
          {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-noninteractive-element-interactions */}
          <img
            style={{ width: '14px', height: '18px' }}
            alt="edit"
            src={editIcon}
            onClick={() => {
              onEdit(record);
            }}
          />
          {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-noninteractive-element-interactions */}
          <img
            style={{ width: '18px', height: '18px', marginLeft: '20px' }}
            alt="restore"
            src={restoreIcon}
            onClick={() => {
              onRestore(record);
            }}
          />

          {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-noninteractive-element-interactions */}
          <img
            style={{ width: '18px', height: '18px', marginLeft: '20px' }}
            alt="restore"
            src={deleteIcon}
            onClick={() => {
              onDel(record);
            }}
          />
        </div>
      ),
    },
  ];
  const onSearch = e => {
    console.log(e.target.value);
    setKeySearch(e.target.value);
  };

  const onRestore = record => {
    console.log('restore', record);
  };

  const [infoGUIDEdit, setInfoGUIDEdit] = useState('');

  const onEdit = record => {
    setInfoGUIDEdit(record.InfoGUID);
    setType(2);
    setIsModalOpen(true);
  };

  const [dataDel, setDataDel] = useState(null);

  const onDel = record => {
    // dispatch(actions.delProduct(record.InfoGUID));
    setDataDel(record);
    setIsModalConfirmOpen(true);
  };

  const showModal = () => {
    setType(1);
    setIsModalOpen(true);
  };
  // eslint-disable-next-line no-shadow
  let numberProduct = 0;
  if (listProduct && listProduct.length !== 0) {
    numberProduct = listProduct.length;
  }
  return (
    <UserManagerLayout>
      <Content>
        <SearchUser
          placeholder="Tìm kiếm theo Mã khách hàng, Họ tên khách hàng và số điện thoại"
          onChange={onSearch}
        />
        <Toolbar>
          <strong style={{ flex: 1 }}> Danh sách thẻ: {numberProduct} </strong>
          <CustomButtonAdd
            onClick={showModal}
            icon={<PlusOutlined style={{ verticalAlign: 'baseline' }} />}
          >
            {' '}
            Thêm mới{' '}
          </CustomButtonAdd>
        </Toolbar>
        <UserTable
          columns={columns}
          dataSource={listProduct}
          pagination={{
            pageSize,
          }}
          scroll={{
            y: 550,
          }}
        />
      </Content>

      {isModalOpen && (
        <ProductAddAndEditModal
          trigger={isModalOpen}
          setTrigger={setIsModalOpen}
          type={type}
          UUID={infoGUIDEdit}
        />
      )}

      {isModalConfirmOpen && (
        <ModalConfirmDel
          trigger={isModalConfirmOpen}
          setTrigger={setIsModalConfirmOpen}
          data={dataDel}
        />
      )}
    </UserManagerLayout>
  );
};

export default ProductManager;
