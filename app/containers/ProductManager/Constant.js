import { REDUX_KEY } from '../../utils/constants';

// eslint-disable-next-line camelcase
export const REQUEST_FAlSE = 'REQUEST_FAlSE';
// export const EDIT_USER = 'EDIT_USER';
// export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS';

export const GET_LIST_PRODUCT = `${
  REDUX_KEY.productManagement
}/GET_LIST_PRODUCT`;
export const GET_LIST_PRODUCT_SUCCESS = `${
  REDUX_KEY.productManagement
}/GET_LIST_USER_SUCCESS`;

export const ADD_PRODUCT = `${REDUX_KEY.productManagement}/ADD_PRODUCT`;

export const ADD_PRODUCT_SUCCESS = `${REDUX_KEY}/ADD_PRODUCT_SUCCESS`;

export const EDIT_PRODUCT_SUCCESS = `${REDUX_KEY}/EDIT_PRODUCT_SUCCESS`;

export const EDIT_PRODUCT = `${REDUX_KEY}/EDIT_PRODUCT`;

export const DEL_PRODUCT = `${REDUX_KEY}/DEL_PRODUCT`;

export const DEL_PRODUCT_SUCCESS = `${REDUX_KEY}/DEL_PRODUCT_SUCCESS`;

export const GET_INFO_PRODUCT = `${REDUX_KEY}GET_INFO_PRODUCT`;

export const GET_INFO_PRODUCT_SUCCESS = `${REDUX_KEY}GET_INFO_PRODUCT_SUCCESS`;
