import { Button, Layout, Table } from 'antd';
import styled from 'styled-components';
import Input from 'antd/es/input/Search';

const CustomButtonAdd = styled(Button)`
  margin-right: 40px;
  color: white;
  background-color: rgb(250, 139, 52);
  border-radius: 8px;
`;

const SearchUser = styled(Input)`
  margin-top: 20px;
  margin-left: 20px;
  width: 50%;

  .ant-input-group .ant-input {
    float: left;
    width: 100%;
    margin-bottom: 0;
    text-align: inherit;
    height: 40px;
    border-radius: 12px;
  }
  .ant-input-search-button {
    padding-top: 0;
    padding-bottom: 0;
    border: none;
    height: 35px;
    margin-left: -40px;
    z-index: 1;
  }

  .ant-input-search > .ant-input-group > .ant-input-group-addon:last-child {
    left: 10px;
    padding: 0;
    border: 0;
  }
`;

const Toolbar = styled.div`
  display: flex;
  margin-top: 20px;
  margin-left: 20px;
  margin-bottom: 20px;
  width: 100%;
`;

const UserManagerLayout = styled.div`
  background-color: white;
  width: 95%;
  height: 100%;
  margin: auto;
`;

const UserTable = styled(Table)`
  margin-left: 20px;
  margin-right: 20px;
  .ant-table-thead > tr > th {
    background-color: #c5ced9;
    height: 77px;
  }

  .ant-table {
    border-top-left-radius: 16px;
    border-top-right-radius: 16px;
  }

  .ant-table > tr {
    height: 54px;
  }
  .ant-table-header {
    border-top-left-radius: 12px;
    border-top-right-radius: 12px;
  }
`;

const CustomLayout = styled(Layout)`
  height: 100%;
  width: 100%;
  .ant-layout-sider-collapsed {
    flex: 0 0 72px !important;
    max-width: 72px !important;
    min-width: 72px !important;
    width: 72px !important;
  }
  //.ant-menu-root.ant-menu-vertical,
  //.ant-menu-root.ant-menu-vertical-left,
  //.ant-menu-root.ant-menu-vertical-right,
  .ant-menu-root.ant-menu-inline {
    box-shadow: none;
    height: 100%;
  }
`;

export {
  SearchUser,
  CustomButtonAdd,
  Toolbar,
  UserManagerLayout,
  UserTable,
  CustomLayout,
};
