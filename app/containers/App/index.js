/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React, { useEffect, useState } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { Header } from 'antd/es/layout/layout';
import { Button, Layout, Menu } from 'antd';
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import Sider from 'antd/es/layout/Sider';
import { useDispatch } from 'react-redux';
import Cookies from 'js-cookie';
import PropTypes from 'prop-types';
import Login from '../Login/LoadableLogin';
import ErrorPage from '../../res/components/ErrorPage';
import GlobalStyle from '../../global-styles';
import { normalTheme } from '../../themes/normalTheme';
import UserManager from '../UserManager/loadableUserManager';
import ProductManager from '../ProductManager/productLoadable';
import history from '../../utils/history';
import CoporationBrand from '../../images/logo.svg';
import LogoutIcon from '../../images/icon/avatarDefault.svg';
import { CustomLayout } from './style';
import AccountManageIcon from '../../images/icon/accountManagementMenu.svg';
import ProductManageIcon from '../../images/icon/productManagementMenu.svg';
import * as actions from './actions';
import reducer from './reducer';
import saga from './saga';
import { useInjectReducer } from '../../utils/injectReducer';
import { useInjectSaga } from '../../utils/injectSaga';
import { REDUX_KEY } from './constants';
import { COOKIES } from '../../utils/constants';

const key = REDUX_KEY;

const RouterNotLogin = ({ path, component: Component }) => {
  const token = Cookies.get(COOKIES.access_token);
  return (
    <Route
      render={() =>
        // eslint-disable-next-line no-nested-ternary
        !token ? (
          path === '/' ? (
            <Redirect to="/login" />
          ) : (
            <Component />
          )
        ) : (
          <Redirect to="/user-management" />
        )
      }
    />
  );
};
RouterNotLogin.propTypes = {
  path: PropTypes.string,
  component: PropTypes.func,
};

const RouterLogin = ({ path, component: Component }) => {
  const token = Cookies.get(COOKIES.access_token);
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const dispatch = useDispatch();
  const MENU_DATA = [
    {
      key: '1',
      icon: (
        <img
          src={AccountManageIcon}
          alt="account"
          style={{ width: '25px', height: '25px' }}
        />
      ),
      label: 'Quản lý tài khoản',
    },
    {
      key: '2',
      icon: (
        <img
          src={ProductManageIcon}
          alt="account"
          style={{ width: '25px', height: '25px' }}
        />
      ),
      label: 'Quản lý sản phẩm',
    },
  ];
  const [collapsed, setCollapsed] = useState(false);
  const onClickMenu = e => {
    switch (e.key) {
      case '1':
        if (token) {
          history.push('/user-management');
        } else {
          history.push('/login');
        }
        break;
      case '2':
        // eslint-disable-next-line no-unused-expressions
        if (token) {
          history.push('/product-management');
        } else {
          history.push('/login');
        }
        break;
      default:
        break;
    }
  };

  const handleLogout = () => {
    dispatch(
      actions.logout(() => {
        Cookies.remove(COOKIES.access_token);
      }),
    );
    history.push('/login');
  };

  useEffect(() => {
    if (!token) {
      history.push('/login');
    }
  }, [token]);

  return (
    <Route
      exact
      path={path}
      render={() => (
        <Layout>
          <Header
            style={{
              background: 'white',
              paddingLeft: 30,
              borderBottom: 'solid 1px black',
            }}
          >
            <div style={{ display: 'flex' }}>
              <div>
                {React.createElement(
                  collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: 'trigger',
                    onClick: () => {
                      setCollapsed(!collapsed);
                    },
                  },
                )}
              </div>

              <div style={{ width: '170px', marginLeft: '25px' }}>
                <img
                  alt="coporationBrand"
                  src={CoporationBrand}
                  style={{
                    margin: 'auto',
                    height: '50px',
                    width: '125px',
                    left: '56px',
                    top: '29px',
                    borderRadius: '0px',
                  }}
                />
              </div>

              <Button
                style={{
                  margin: 'auto',
                  marginRight: '10px',
                  border: 'none',
                }}
                onClick={handleLogout}
              >
                <img
                  alt="logout"
                  src={LogoutIcon}
                  style={{ height: '30px', width: '30x' }}
                />
              </Button>
            </div>
          </Header>
          <CustomLayout style={{ display: 'flex', flexDirection: 'row' }}>
            <Sider trigger={null} collapsed={collapsed}>
              <Menu mode="inline" items={MENU_DATA} onClick={onClickMenu} />
            </Sider>
            <div style={{ width: '100%', background: 'white' }}>
              <Component />
            </div>
          </CustomLayout>
        </Layout>
      )}
    />
  );
};

RouterLogin.propTypes = {
  path: PropTypes.string,
  component: PropTypes.func,
};

export default function App() {
  return (
    <ThemeProvider theme={normalTheme}>
      <>
        <Switch>
          <RouterNotLogin exact path="/login" component={Login} />
          <RouterNotLogin exact path="/" component={Login} />
          <RouterLogin path="/user-management" component={UserManager} />
          <RouterLogin path="/product-management" component={ProductManager} />
          <Route path="" render={() => <ErrorPage code="404" />} />
        </Switch>
        <GlobalStyle />
      </>
    </ThemeProvider>
  );
}
