import { createSelector } from 'reselect';
import { REDUX_KEY } from './constants';
import { initialState } from './reducer';
export const selectLogout = state => state[REDUX_KEY] || initialState;
export const selectLoading = () =>
  createSelector(
    selectLogout(),
    state => state.isLoading,
  );
