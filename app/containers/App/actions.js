// eslint-disable-next-line no-unused-vars
import { LOGOUT, LOGOUT_SUCCESS } from './constants';

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function logoutsuccess() {
  return {
    type: LOGOUT_SUCCESS,
  };
}
