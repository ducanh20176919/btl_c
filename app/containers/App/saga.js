import { call, takeLatest } from 'redux-saga/effects';

import Cookies from 'js-cookie';
import * as constants from './constants';
import { axiosPost } from '../../utils/request';
import history from '../../utils/history';
import { COOKIES } from '../../utils/constants';

export function* handleLogout(action) {
  const path = 'user/logout';
  try {
    const res = yield call(axiosPost, path);
    if (res.status === 200) {
      Cookies.remove(COOKIES.access_token);
      history.push('/login');
      action.callback();
    }
  } catch (error) {
    history.push('/');
    console.log('error');
  }
}

export default function* watchLogout() {
  yield takeLatest(constants.LOGOUT, handleLogout);
}
