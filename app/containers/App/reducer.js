import produce from 'immer';
import { LOGOUT, LOGOUT_SUCCESS } from './constants';

export const initialState = {
  isLoading: false,
};

const logoutReducer = (state = initialState, action) =>
  produce(state, draft => {
    // eslint-disable-next-line default-case
    switch (action.type) {
      case LOGOUT:
        // eslint-disable-next-line no-param-reassign
        draft.isLoading = true;
        break;
      case LOGOUT_SUCCESS:
        // eslint-disable-next-line no-param-reassign
        draft.isLoading = false;
        break;
    }
  });

export default logoutReducer;
