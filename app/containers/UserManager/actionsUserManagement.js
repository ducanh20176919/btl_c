import * as CONSTANT from './constantsUserManagement';

export function requestFalse() {
  return {
    type: CONSTANT.REQUEST_FAlSE,
  };
}

export function getListUser(body, callback) {
  return {
    type: CONSTANT.GET_LIST_USER,
    body,
    callback,
  };
}

export function getListUserSuccess(data) {
  return {
    type: CONSTANT.GET_LIST_USER_SUCCESS,
    data,
  };
}

export function addUser(body, callback) {
  return {
    type: CONSTANT.ADD_USER,
    body,
    callback,
  };
}

export function addUserSuccess(data) {
  return {
    type: CONSTANT.ADD_USER_SUCCESS,
    data,
  };
}

export function editUser(body, callback) {
  return {
    type: CONSTANT.EDIT_USER,
    body,
    callback,
  };
}

export function editUserSuccess(data) {
  return {
    type: CONSTANT.EDIT_USER_SUCCESS,
    data,
  };
}

export function getDropdown() {
  return {
    type: CONSTANT.GET_DROPDOWN,
  };
}

export function getDropDownSuccess(data) {
  return {
    type: CONSTANT.GET_DROPDOWN_SUCCESS,
    data,
  };
}

export function getInfoUser(uuID) {
  return {
    type: CONSTANT.GET_DATA_USER_TO_EDIT,
    uuID,
  };
}

export function getInfoUserSuccess(data) {
  return {
    type: CONSTANT.GET_DATA_USER_TO_EDIT_SUCCESS,
    data,
  };
}
