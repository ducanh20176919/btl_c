/**
 * ...
 */

import produce from 'immer';
import {
  ADD_USER,
  ADD_USER_SUCCESS,
  EDIT_USER,
  EDIT_USER_SUCCESS,
  GET_DATA_USER_TO_EDIT,
  GET_DATA_USER_TO_EDIT_SUCCESS,
  GET_DROPDOWN,
  GET_DROPDOWN_SUCCESS,
  GET_LIST_USER,
  GET_LIST_USER_SUCCESS,
} from './constantsUserManagement';

export const initialState = {
  isLoading: false,
  isLoadDropdownLoad: true,
  totalRecord: 0,
  listUser: [],
  isAddedSuccess: false,
  dropDownList: [],
  isEditSuccess: false,
  infoUser: [],
};

/* eslint-disable default-case, no-param-reassign */
const userManagementReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_LIST_USER:
        draft.isLoading = true;
        break;
      case ADD_USER:
        draft.isLoading = true;
        break;
      case GET_DROPDOWN:
        draft.isLoading = true;
        draft.isLoadDropdownLoad = true;
        break;
      case EDIT_USER:
        draft.isLoading = true;
        break;
      case GET_DATA_USER_TO_EDIT:
        draft.isLoading = true;
        break;
      case GET_LIST_USER_SUCCESS:
        draft.listUser = action.data.data.Users;
        draft.isLoading = false;
        break;
      case ADD_USER_SUCCESS:
        draft.isAddedSuccess = true;
        draft.isLoading = false;
        break;
      case GET_DROPDOWN_SUCCESS:
        draft.dropDownList = action.data.data.Dropdown;
        draft.isLoadDropdownLoad = false;
        draft.isLoading = false;
        break;
      case EDIT_USER_SUCCESS:
        draft.isAddedSuccess = true;
        draft.isLoading = false;
        break;
      case GET_DATA_USER_TO_EDIT_SUCCESS:
        draft.infoUser = action.data.data;
        draft.isLoading = false;
        break;
    }
  });

export default userManagementReducer;
