import { REDUX_KEY } from '../../utils/constants';

// eslint-disable-next-line camelcase
export const REQUEST_FAlSE = 'REQUEST_FAlSE';

export const EDIT_USER = `${REDUX_KEY}EDIT_USER`;
export const EDIT_USER_SUCCESS = `${REDUX_KEY}EDIT_USER_SUCCESS`;

export const GET_LIST_USER = `${REDUX_KEY.accountManagement}/GET_LIST_ACCOUNT`;
export const GET_LIST_USER_SUCCESS = `${
  REDUX_KEY.accountManagement
}/GET_LIST_USER_SUCCESS`;

export const ADD_USER = `${REDUX_KEY.accountManagement}/ADD_USER`;
export const ADD_USER_SUCCESS = `${
  REDUX_KEY.accountManagement
}/ADD_USER_SUCCESS`;

export const GET_DROPDOWN = `${REDUX_KEY}/DROPDOWN`;
export const GET_DROPDOWN_SUCCESS = `${REDUX_KEY}/DROP_DOWN_SUCCESS`;

export const GET_DATA_USER_TO_EDIT = `${REDUX_KEY}GET_DATA_USER_TO_EDIT`;
export const GET_DATA_USER_TO_EDIT_SUCCESS = `${REDUX_KEY}GET_DATA_USER_TO_EDIT_SUCCESS`;
