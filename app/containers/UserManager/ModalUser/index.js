import React, { useEffect } from 'react';
import { DatePicker, Form, Radio, Select } from 'antd';

import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import {
  ButtonAdd,
  CustomBtns,
  ButtonCancel,
  CustomSelect,
  CustomModal,
} from './style';
import InputCustom from '../../Component/Input';
import { useInjectReducer } from '../../../utils/injectReducer';
import reducer from '../reducerUserManagement';
import { useInjectSaga } from '../../../utils/injectSaga';
import saga from '../sagaUserManagement';
import { REDUX_KEY } from '../../../utils/constants';
import * as actions from '../actionsUserManagement';
import * as selector from '../selectorsUserManagement';
const key = REDUX_KEY.accountManagement;

// eslint-disable-next-line react/prop-types
const ModalAddUser = ({ trigger, setTrigger, type, data, PC }) => {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actions.getDropdown());
  }, []);

  const listDropdown = useSelector(selector.selectDropDown());

  useEffect(() => {
    if (data) {
      dispatch(actions.getInfoUser(data));
    }
  }, [trigger, data]);

  const InfoUser = useSelector(selector.selectInfoUser());

  const handleSubmit = () => {
    // eslint-disable-next-line no-shadow
    // const newUser = form.getFieldsValue();
    // eslint-disable-next-line no-shadow
    form.validateFields().then(value => {
      if (type === 1) {
        dispatch(
          actions.addUser({ ...value, RoleID: 3 }, () => {
            console.log('loading new table');
            // goi lai list tai day
          }),
        );
      }
      if (type === 2 && InfoUser) {
        let body = {
          ...value,
          UserGUID: data,
          RoleID: 3,
          Birthday: '2022-12-06T04:34:17',
        };
        if (body.HierarchyGUID === undefined) {
          const hierachy = listDropdown.filter(item => item.Name === PC).at(0)
            .HierarchyGUID;
          body = {
            ...body,
            HierarchyGUID: hierachy,
          };
        }
        dispatch(
          actions.editUser(body, () => {
            console.log('loading new table');
            // Goi lai list tai day
          }),
        );
      }
    });
    setTrigger(false);
  };

  const handleCancel = () => {
    setTrigger(false);
  };

  useEffect(() => {
    if (type === 1) {
      form.setFieldsValue({
        UserName: '',
        FullName: '',
        Phone: '',
        Email: '',
        Address: '',
        IsActive: '',
        Birthday: moment(new Date()),
      });
    } else if (InfoUser && type === 2) {
      form.setFieldsValue({
        UserName: InfoUser.UserName,
        FullName: InfoUser.FullName,
        Phone: InfoUser.Phone,
        Email: InfoUser.Email,
        Address: InfoUser.Address,
        IsActive: InfoUser.IsActive,
        Birthday: moment(InfoUser.Birthday),
      });
    }
  }, [InfoUser, type]);

  return (
    <div>
      <CustomModal
        title={type === 1 ? 'Thêm mới tài khoản' : 'Sửa thông tin tài khoản'}
        onCancel={handleCancel}
        open={trigger}
        footer={null}
        visible={trigger}
      >
        <CustomSelect form={form}>
          <Form.Item name="UserName">
            <InputCustom
              label={
                <span>
                  Tài khoản<span style={{ color: 'red' }}>*</span>
                </span>
              }
              placeholder="UserName"
            />
          </Form.Item>

          <Form.Item name="FullName">
            <InputCustom
              label={
                <span>
                  Tên đầy đủ<span style={{ color: 'red' }}>*</span>
                </span>
              }
              placeholder="Nhập tên"
            />
          </Form.Item>

          <Form.Item name="Phone">
            <InputCustom
              label={
                <span>
                  Số điện thoại<span style={{ color: 'red' }}>*</span>
                </span>
              }
              placeholder="Nhập số điện thoại"
            />
          </Form.Item>
          <Form.Item name="Email">
            <InputCustom
              label={
                <span>
                  Email<span style={{ color: 'red' }}>*</span>
                </span>
              }
              placeholder="Nhập Email"
            />
          </Form.Item>
          <Form.Item name="Address">
            <InputCustom
              label={
                <span>
                  Địa chỉ<span style={{ color: 'red' }}>*</span>
                </span>
              }
              placeholder="địa chỉ"
            />
          </Form.Item>
          <Form.Item name="Birthday">
            <DatePicker format="DD/MM/YYYY" />
          </Form.Item>
          <Form.Item name="HierarchyGUID">
            <Select
              defaultValue={PC}
              label={
                <span>
                  Nhóm đại lý<span style={{ color: 'red' }}>*</span>
                </span>
              }
            >
              {listDropdown &&
                listDropdown.map(item => (
                  <Select.Option value={item.HierarchyGUID}>
                    {item.Name}
                  </Select.Option>
                ))}
            </Select>
          </Form.Item>
          <Form.Item name="IsActive" label={<strong>Trạng thái</strong>}>
            <Radio.Group>
              <Radio value>Đang hoạt động</Radio>
              <Radio value={false}>Không hoạt động</Radio>
            </Radio.Group>
          </Form.Item>
        </CustomSelect>
        <CustomBtns>
          <ButtonAdd onClick={handleSubmit}>Ghi lại (Ctrl + S)</ButtonAdd>
          <ButtonCancel onClick={handleCancel}>Đóng (ESC)</ButtonCancel>
        </CustomBtns>
      </CustomModal>
    </div>
  );
};
export default ModalAddUser;
