import { call, put, takeLatest } from 'redux-saga/effects';

import * as constants from './constantsUserManagement';
import * as actions from './actionsUserManagement';

import { axiosGet, axiosPost, axiosPut } from '../../utils/request';

export function* handleGetList(action) {
  const path = `user/list`;
  try {
    const res = yield call(axiosPost, path, action.body);
    if (res.status === 200) {
      yield put(actions.getListUserSuccess(res));
    }
  } catch (error) {
    yield put(actions.requestFalse(error));
  }
}

export function* addUser(action) {
  const path = `user/v2/add`;
  try {
    const res = yield call(axiosPost, path, action.body);
    if (res.status === 200) {
      action.callback();
    }
    if (res.status === 400) {
      alert('errr');
    }
  } catch (error) {
    yield put(actions.requestFalse(error));
  }
}

export function* getDropDown() {
  const path = `hierarchy/dropdown`;
  try {
    const res = yield call(axiosPost, path);
    if (res.status === 200) {
      yield put(actions.getDropDownSuccess(res));
    }
  } catch (error) {
    yield put(actions.requestFalse(error));
  }
}

export function* getInfoUser(action) {
  const path = `user/information?UserGUID=${action.uuID}`;
  try {
    const res = yield call(axiosGet, path);
    if (res.status === 200) {
      yield put(actions.getInfoUserSuccess(res));
    }
  } catch (error) {
    yield put(actions.requestFalse(error));
  }
}

export function* editUser(action) {
  const path = `user/v2/editmember`;
  try {
    const res = yield call(axiosPut, path, action.body);
    if (res.status === 200) {
      action.callback();
    }
  } catch (error) {
    yield put(actions.requestFalse(error));
  }
}

export default function* watchLogin() {
  yield takeLatest(constants.GET_LIST_USER, handleGetList);
  yield takeLatest(constants.ADD_USER, addUser);
  yield takeLatest(constants.EDIT_USER, editUser);
  yield takeLatest(constants.GET_DROPDOWN, getDropDown);
  yield takeLatest(constants.GET_DATA_USER_TO_EDIT, getInfoUser);
}
