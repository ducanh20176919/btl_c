import { createSelector } from 'reselect';
import { REDUX_KEY } from '../../utils/constants';
import { initialState } from './reducerUserManagement';

export const selectAccountManagement = state =>
  state[REDUX_KEY.accountManagement] || initialState;

export function selectLoading() {
  return createSelector(
    selectAccountManagement,
    state => state.isLoading,
  );
}

export function selectLoadingDropdown() {
  return createSelector(
    selectAccountManagement,
    state => state.isLoadDropdownLoad,
  );
}

export function selectListUser() {
  return createSelector(
    selectAccountManagement,
    state => state.listUser,
  );
}

export function selectIsAdded() {
  return createSelector(
    selectAccountManagement,
    state => state.isAddedSuccess,
  );
}

export function selectDropDown() {
  return createSelector(
    selectAccountManagement,
    state => state.dropDownList,
  );
}

export function selectIsEdited() {
  return createSelector(
    selectAccountManagement,
    state => state.isEditSuccess,
  );
}

export function selectInfoUser() {
  return createSelector(
    selectAccountManagement,
    state => state.infoUser,
  );
}
