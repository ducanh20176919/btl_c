import React, { useEffect, useState } from 'react';
import { PlusOutlined } from '@ant-design/icons';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import {
  UserTable,
  SearchUser,
  Toolbar,
  UserManagerLayout,
  CustomButtonAdd,
} from '../App/style';

import ModalAddUser from './ModalUser';
import editIcon from '../../images/icon/edit.svg';
import restoreIcon from '../../images/icon/iconRestore.svg';
import * as actions from './actionsUserManagement';
import * as selector from './selectorsUserManagement';
import { useInjectReducer } from '../../utils/injectReducer';
import reducer from './reducerUserManagement';
import { useInjectSaga } from '../../utils/injectSaga';
import saga from './sagaUserManagement';
import { REDUX_KEY } from '../../utils/constants';

const body = {
  KeySearch: '',
  Phone: '',
  RoleGroupID: 0,
  HierachyID: 0,
  ListUserID: ['0'],
  PageSize: 50,
  CurrentPage: 1,
  SortCol: '',
  IsDesc: false,
};

const key = REDUX_KEY.accountManagement;

const UserManager = () => {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });
  const dispatch = useDispatch();
  const listUser = useSelector(selector.selectListUser());

  // eslint-disable-next-line no-unused-expressions,array-callback-return
  listUser &&
    // eslint-disable-next-line array-callback-return
    listUser.map(item => {
      const date = moment(new Date(item.Birthday));
      // eslint-disable-next-line no-param-reassign
      item.Birthday = date.format('DD/MM/YYYY');
    });

  const columns = [
    {
      title: 'STT',
      width: '4%',
      render: record => (
        <div>{(Number(listUser.indexOf(record)) % 10) + 1}</div>
      ),
    },
    {
      title: 'Tên đăng nhập',
      dataIndex: 'UserName',
      width: '9%',
    },
    {
      title: 'Tên đầy đủ',
      dataIndex: 'FullName',
      width: '10%',
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'Phone',
      width: '10%',
    },
    {
      title: 'Email',
      dataIndex: 'Email',
      width: '15%',
    },
    {
      title: 'Ngày sinh',
      dataIndex: 'Birthday',
      width: '13%',
    },
    {
      title: 'Nhóm quyền',
      dataIndex: 'RoleGroupName',
      width: '13%',
    },
    {
      title: 'Phân cấp',
      dataIndex: 'HierarchyName',
      width: '10%',
    },
    {
      width: '10%',
      title: 'Chức năng',
      dataIndex: 'key',
      render: (text, record) => (
        <div>
          {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-noninteractive-element-interactions */}
          <img
            style={{ width: '14px', height: '18px' }}
            alt="edit"
            src={editIcon}
            onClick={() => {
              onEdit(record);
            }}
          />
          <img
            style={{ width: '18px', height: '18px', marginLeft: '20px' }}
            alt="restore"
            src={restoreIcon}
          />
        </div>
      ),
    },
  ];

  const [typeModal, setTypeModal] = useState(1);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [uuID, setUUID] = useState('');
  const [PC, setPC] = useState('');
  const [keySearch, setKeySearch] = useState('');

  const renderListUser = dataInput => {
    dispatch(actions.getListUser(dataInput));
  };

  useEffect(() => {
    const time = setTimeout(() => {
      renderListUser({
        ...body,
        KeySearch: keySearch,
      });
    }, 300);
    return () => clearTimeout(time);
  }, [keySearch, isModalOpen]);

  const openModalAdd = () => {
    setIsModalOpen(true);
    setTypeModal(1);
    setUUID('');
  };

  const onSearch = e => {
    const valueSearch = e.target.value;
    setKeySearch(valueSearch);
  };

  const onEdit = data => {
    setTypeModal(2);
    setIsModalOpen(true);
    setUUID(data.UserGUID);
    setPC(data.HierarchyName);
  };

  let numberAcc = 0;
  if (listUser && listUser.length !== 0) {
    numberAcc = listUser.length;
  }

  return (
    <UserManagerLayout>
      {/* <Content> */}
      <SearchUser
        placeholder="Tìm kiếm theo tên đăng nhập, tên đầy đủ, số điện thoại"
        onChange={onSearch}
      />
      <Toolbar>
        <strong style={{ flex: 1 }}> Quản lý tài khoản: {numberAcc} </strong>
        <CustomButtonAdd
          onClick={openModalAdd}
          icon={<PlusOutlined style={{ verticalAlign: 'baseline' }} />}
        >
          {' '}
          Thêm mới{' '}
        </CustomButtonAdd>
      </Toolbar>
      <UserTable
        // loading={isLoading}
        columns={columns}
        dataSource={listUser}
        pagination={{
          pageSize: 10,
        }}
        scroll={{
          y: 550,
        }}
      />
      {isModalOpen && (
        <ModalAddUser
          trigger={isModalOpen}
          setTrigger={setIsModalOpen}
          type={typeModal}
          data={uuID}
          PC={PC}
        />
      )}
    </UserManagerLayout>
  );
};

export default UserManager;
