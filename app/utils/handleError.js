import { NotificationPopup } from '../res/components/NotificationPopup';
import { ERROR } from '../res/components/NotificationPopup/constants';
import history from './history';

export const handleError = errorBody => {
  const { message } = errorBody.data;
  console.log(message);
  const code = errorBody.status;

  switch (code) {
    case 400:
      NotificationPopup(message, ERROR);
      break;
    case 401:
      history.push('/login');
      NotificationPopup(message, ERROR);
      break;
    case 404:
      NotificationPopup(message, ERROR);
      break;
    default:
      NotificationPopup(message, ERROR);
      break;
  }
};
