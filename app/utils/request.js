import axios from 'axios';
import Cookies from 'js-cookie';
import { handleError } from './handleError';
import { NotificationPopup } from '../res/components/NotificationPopup';
import { ERROR } from '../res/components/NotificationPopup/constants';
import { COOKIES } from './constants';

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  if (response.status === 401) {
    NotificationPopup(response.message, ERROR);
    return response.message;
  }

  if (response.status === 400) {
    handleError(response.message);
    return response.message;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

const instance = axios.create({
  baseURL: `${
    process.env.NODE_ENV === 'production'
      ? window.SystemConfig.URL
      : 'https://apiedl.bkav.com/'
  }api/`,
});

instance.defaults.timeout = 25000;

instance.interceptors.request.use(req => {
  req.headers.Authorization = `bearer ${Cookies.get(COOKIES.access_token)}`;
  return req;
});

instance.interceptors.response.use(
  response => response,
  error => {
    const responseError = {
      ...error,
      response: {
        ...error.response,
      },
    };

    if (error.response) {
      handleError(error.response);
    }

    return responseError;
  },
);

export async function axiosGet(path) {
  const res = await instance
    .get(path)
    .then(checkStatus)
    .catch(error => {
      if (!JSON.parse(JSON.stringify(error)).response) throw error;
    });
  return res;
}

export async function axiosPut(path, body) {
  const res = await instance
    .put(path, body)
    .then(checkStatus)
    .catch(error => {
      if (!JSON.parse(JSON.stringify(error)).response) throw error;
    });
  return res;
}

export async function axiosDelete(path) {
  const res = await instance
    .delete(path)
    .then(checkStatus)
    .catch(error => {
      if (!JSON.parse(JSON.stringify(error)).response) throw error;
    });
  return res;
}

export async function axiosPost(path, body) {
  const res = await instance
    .post(path, body)
    .then(checkStatus)
    .catch(error => {
      if (!JSON.parse(JSON.stringify(error)).response) throw error;
    });
  return res;
}
