import React, { useState } from 'react';

export const AppContext = React.createContext(null);

// eslint-disable-next-line react/prop-types
export default function AppProvider({ children }) {
  const [authToken, setAuthToken] = useState('');
  const store = {
    accessToken: [authToken, setAuthToken],
  };
  return <AppContext.Provider value={store}>{children}</AppContext.Provider>;
}
